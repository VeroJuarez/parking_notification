package parking.notification;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import parking.notification.Sender;

public class NotificationWhatsapp implements Sender {
	public String accountSid;
	public String authToken;
	
	public NotificationWhatsapp(String accountSid, String authToken) {
		this.accountSid=accountSid;
		this.authToken=authToken;
	}

	public boolean send(String number, String mensaje) {
		String numeroEmisor= "whatsapp:+14155238886";;
		String caracteristica="whatsapp:";;
		
		try {
			Twilio.init(accountSid,authToken);
			Message.creator(
					new PhoneNumber(caracteristica+ "+54" + number),
					new PhoneNumber(numeroEmisor), mensaje).create();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
